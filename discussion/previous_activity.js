// [Section] Activity
// This route expects to receive a GET request at the URI "/home"
app.get("/home", (req, res) => {
    res.send("Welcome to the home page");
})

// This route expects to receive a GET request at the URI "/users"
// This will retrieve all the users stored in the variable created above
app.get("/users", (req, res) => {
    res.send(users);
})

// This route expects to receive a DELETE request at the URI "/delete-user"
// This will remove the user from the array for deletion
app.delete("/delete-user", (req, res) => {

    // Creates a variable to store the message to be sent back to the client/Postman 
    let message;

    /*
        .pop(users)
        aLEX - 0
        BEn -1 
 
    */

    // Creates a condition if there are users found in the array
    if (users.length != 0){

        // Creates a for loop that will loop through the elements of the "users" array
        for(let i = 0; i < users.length; i++){

            // If the username provided in the client/Postman and the username of the current object in the loop is the same
            if (req.body.username == users[i].username) {

                // The splice method manipulates the array and removes the user object from the "users" array based on it's index
                // users[i] is used here to indicate the start of the index number in the array for the element to be removed
                // The number 1 defines the number of elements to be removed from the array
                users.splice(users[i], 1);

                // Changes the message to be sent back by the response
                message = `User ${req.body.username} has been deleted.`;

                break;

            } 

        }

    // If no user was found
    } else {

        // Changes the message to be sent back by the response
        message = "User does not exist.";

    }
    
    // Sends a response back to the client/Postman once the user has been deleted or if a user is not found
    res.send(message);

})