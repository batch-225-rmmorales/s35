const express = require("express");
const mongoose = require("mongoose");


const app = express();
const port = 5000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));
let db = mongoose.connection;

mongoose.connect("mongodb+srv://mors2:N0rmanrafael@cluster0.uvgjod2.mongodb.net/s35?retryWrites=true&w=majority", 
//options in braces
{	
    // In simple words, "useNewUrlParser : true" allows us to avoid any current and future errors while connecting to MongoDB
    useNewUrlParser : true,

    // False by default. Set to true opt in to using MongoDB driver's new connection management engine. You shoult set this option to true, except for the unlikely case that it prevents you from mainting a stable connection
    useUnifiedTopology : true
}
);

// set notification for connection success or failure in mongo db

db.on("error", console.error.bind(console,"connection error"));

db.on("open", () => console.log("connected to mongodb"));


const userSchema = new mongoose.Schema({ 
	// Define the fields with the corresponding data type
	// For a task, it needs a "task name" and "task status"
	// There is a field called "name" and its data type is "String"
	username : String,
    password: String,
	// There is a field called "status" that is a "String" and the default value is "pending"
	status : { 
		type : String,
		// Default values are the predefined values for a field if we don't put any value
		default : "pending"
	}
});

//models
// uses schema and are to create instantiate objects that correspond to schema
//models use schema and they act as the middleman from the server (JS CODE) to our database

// variable task can now use to run commands to interact with database
// task is capitalized following the MVC(model view and control) approach for naming convertion
//models must be in singular form and capitalized
//for the first parameter indicates the collection in where to store the data)

//2nd parameter is to specify the schema/blueprint
// mongoose can create plural form

const User = mongoose.model("User", userSchema);

app.post("/signup", (req,res)=>{
    // Check if there are duplicate tasks
	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
	// findOne() returns the first document that matches the search criteria
	// If there are no matches, the value of result is null
	// "err" is a shorthand naming convention for errors
	User.findOne({username : req.body.username}, (err, result) => {

		// If a document was found and the document's name matches the information sent via the client/Postman
		if(result != null && result.username == req.body.username){

			// Return a message to the client/Postman
			return res.send("Duplicate user found");

		// If no document was found
		} else {

			// Create a new task and save it to the database
			let newUser = new User({
				username : req.body.username,
                password : req.body.password
			});

			// The "save" method will store the information to the database
			// Since the "newTask" was created/instantiated from the Mongoose Schema it will gain access to this method to save to the database
			// The "save" method will accept a callback function which stores any errors found in the first parameter
			// The second parameter of the callback function will store the newly saved document
			// Call back functions in mongoose methods are programmed this way to store any errors in the first parameter and the returned results in the second parameter
			newUser.save((saveErr, savedUser) => {

				// If there are errors in saving
				if(saveErr){

					// Will print any errors found in the console
					// saveErr is an error object that will contain details about the error
					// Errors normally come as an object data type
					return console.error(saveErr);

				// No error found while creating the document
				} else {

					// Return a status code of 201 for created
					// Sends a message "New task created" on successful creation
					return res.status(201).send("New User created");

				}
			})
		}

	})
});






app.listen(port, () => console.log(`listening port ${port}`));

